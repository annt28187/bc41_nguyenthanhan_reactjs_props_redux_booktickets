import { combineReducers } from 'redux';
import { bookTicketReducer } from './bookTicketReducer';

export const rootReducer = combineReducers({
  bookTicketReducer: bookTicketReducer,
});
