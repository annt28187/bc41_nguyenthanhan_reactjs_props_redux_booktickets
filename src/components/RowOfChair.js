import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bookChairAction } from './redux/action/bookTicketAction';

class RowOfChair extends Component {
  renderChair = () => {
    return this.props.row.danhSachGhe.map((item, index) => {
      let cssGheDaDat = '';
      let disabled = false;
      //trạng thái ghế đã bị đặt
      if (item.daDat) {
        cssGheDaDat = 'gheDuocChon';
        disabled = true;
      }
      //xét trạng thái đặt
      let cssGheDangDat = '';
      let indexGheDangDat = this.props.listChair.findIndex(
        (gheDangDat) => gheDangDat.soGhe === item.soGhe
      );
      if (indexGheDangDat !== -1) {
        cssGheDangDat = 'gheDangChon';
      }

      return (
        <button
          disabled={disabled}
          className={`ghe ${cssGheDaDat} ${cssGheDangDat}`}
          key={index}
          onClick={() => {
            this.props.handleBookChair(item);
          }}
        >
          {item.soGhe}
        </button>
      );
    });
  };

  render() {
    return (
      <div className="text-light text-left ml-5 mt-3">
        {this.props.row.hang}
        {this.renderChair()}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listChair: state.bookTicketReducer.listChair,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleBookChair: (chair) => {
      dispatch(bookChairAction(chair));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RowOfChair);
