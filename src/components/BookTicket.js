import React, { Component } from 'react';
import './BookTicket.css';
import { dataChairs } from './data_chairs';
import InfoBookChair from './InfoBookChair';
import RowOfChair from './RowOfChair';

export default class BookTicket extends Component {
  renderHangGhe = () => {
    return dataChairs.map((row, index) => {
      return (
        <div key={index}>
          <RowOfChair row={row} />
        </div>
      );
    });
  };

  render() {
    return (
      <div
        className="bookingMovie"
        style={{
          position: 'fixed',
          width: '100%',
          height: '100%',
          backgroundImage: 'url(./img/bgmovie.jpg)',
          backgroundSize: '100%',
        }}
      >
        <div
          style={{
            position: 'fixed',
            backgroundColor: 'rgba(0,0,0,0.7)',
            height: '100%',
            width: '100%',
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-8 text-center">
                <h1 className="text-warning mt-2">ĐẶT VÉ XEM PHIM</h1>
                <div className="mt-3 text-light" style={{ fontSize: '25px' }}>
                  Màn hình
                </div>
                <div
                  className="mt-1"
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}
                >
                  <div className="screen"></div>
                </div>
                {this.renderHangGhe()}
              </div>
              <div className="col-4 ">
                <h3 className="text-light mt-3">DANH SÁCH GHẾ BẠN CHỌN</h3>
                <InfoBookChair />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
