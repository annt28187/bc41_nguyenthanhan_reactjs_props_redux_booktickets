let initialValue = {
import { BOOK_CHAIR } from './../constant/bookTicketConstant';
  listChair: [],
};

export const bookTicketReducer = (state = initialValue, action) => {
  switch (action.type) {
    case BOOK_CHAIR: {
      let cloneChair = [...state.listChair];
      let index = cloneChair.findIndex((chair)=>{
        return chair.soGhe === action.payload.soGhe;
      })
      if(index!== -1){
        cloneChair.splice(index,1);
      } else{
        cloneChair.push(action.item)
      }
      return {...state, listChair: cloneChair}
    }
    default:
      return state;
  }
};
