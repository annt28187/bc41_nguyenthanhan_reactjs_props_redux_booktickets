import { BOOK_CHAIR, CANCEL_CHAIR } from './../constant/bookTicketConstant';
let initialValue = {
  listChair: [],
};

export const bookTicketReducer = (state = initialValue, action) => {
  switch (action.type) {
    case BOOK_CHAIR: {
      let cloneChair = [...state.listChair];
      let index = cloneChair.findIndex((chair) => {
        return chair.soGhe === action.payload.soGhe;
      });
      if (index !== -1) {
        cloneChair.splice(index, 1);
      } else {
        cloneChair.push(action.item);
      }
      return { ...state, listChair: cloneChair };
    }
    case CANCEL_CHAIR: {
      let cloneChair = [...state.listChair];
      let index = cloneChair.findIndex((chair) => {
        return chair.soGhe === action.soGhe;
      });
      if (index !== -1) {
        cloneChair.splice(index, 1);
      }
      return { ...state, listChair: cloneChair };
    }
    default:
      return state;
  }
};
