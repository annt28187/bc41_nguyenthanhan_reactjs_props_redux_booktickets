import { BOOK_CHAIR, CANCEL_CHAIR } from '../constant/bookTicketConstant';

export const bookChairAction = (chair) => ({
  type: BOOK_CHAIR,
  payload: chair,
});

export const cancelChairAction = (chair) => ({
  type: CANCEL_CHAIR,
  payload: chair,
});
