import React, { Component } from 'react';
import { connect } from 'react-redux';

class InfoBookChair extends Component {
  render() {
    return (
      <div>
        <div className="mt-5">
          <button className="gheDuocChon"></button>
          <span className="text-light ml-2" style={{ fontSize: '20px' }}>
            Ghế đã đặt
          </span>
          <br />
          <button className="gheDangChon"></button>
          <span className="text-light ml-2" style={{ fontSize: '20px' }}>
            Ghế đang đặt
          </span>
          <br />
          <button className="ghe ml-0"></button>
          <span className="text-light ml-2" style={{ fontSize: '20px' }}>
            Ghế chưa đặt
          </span>
        </div>
        <div className="mt-5">
          <table className="table" border="2">
            <thead>
              <tr className="text-light" style={{ fontSize: '20px' }}>
                <th>Số ghế</th>
                <th>Giá</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.props.listChair.map((gheDangDat, index) => {
                return (
                  <tr key={index} className="text-warning">
                    <td>{gheDangDat.soGhe}</td>
                    <td>{gheDangDat.gia.toLocaleString()}</td>
                    <td>
                      <button
                        className="btn btn-danger"
                        onClick={() => {
                          this.props.dispatch(huyGheAction(gheDangDat.soGhe));
                        }}
                      >
                        Huỷ
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tfoot className="text-danger">
              <tr>
                <td></td>
                <td>Tổng tiền</td>
                <td>
                  {this.props.listChair
                    .reduce((tongTien, gheDangDat, index) => {
                      return (tongTien += gheDangDat.gia);
                    }, 0)
                    .toLocaleString()}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listChair: state.bookTicketReducer.listChair,
  };
};

export default connect(mapStateToProps)(InfoBookChair);
