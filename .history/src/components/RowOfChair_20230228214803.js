import React, { Component } from 'react';

export default class RowOfChair extends Component {
  renderChair = () => {
    return this.props.hangGhe.danhSachGhe.map((item, index) => {
      let cssGheDaDat = '';
      let disabled = false;
      //trạng thái ghế đã bị đặt
      if (item.daDat) {
        cssGheDaDat = 'gheDuocChon';
        disabled = true;
      }
      //xét trạng thái đặt
      let cssGheDangDat = '';
      let indexGheDangDat = this.props.danhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.soGhe === item.soGhe
      );
      if (indexGheDangDat !== -1) {
        cssGheDangDat = 'gheDangChon';
      }

      return (
        <button
          disabled={disabled}
          className={`ghe ${cssGheDaDat} ${cssGheDangDat}`}
          key={index}
          onClick={() => {
            this.props.datGhe(item);
          }}
        >
          {item.soGhe}
        </button>
      );
    });
  };

  render() {
    return (
      <div className="text-light text-left ml-5 mt-3">
        {this.props.hangGhe.hang}
        {this.renderChair()}
      </div>
    );
  }
}
