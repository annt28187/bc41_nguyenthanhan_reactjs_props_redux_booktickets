import logo from './logo.svg';
import './App.css';
import BookTicket from './components/BookTicket';

function App() {
  return (
    <div className="App">
      <BookTicket />
    </div>
  );
}

export default App;
